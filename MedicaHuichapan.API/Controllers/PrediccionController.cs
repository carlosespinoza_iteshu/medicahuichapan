﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MedicaHuichapan.BIZ;
namespace MedicaHuichapan.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrediccionController : ControllerBase
    {
        [HttpPost]
        public double POST(int edad, double peso, double altura, int numHijos, bool esFumador, char sexo, int region)
        {
            PrediccionGastoMedico prediccion = new PrediccionGastoMedico()
            {
                Sexo = sexo,
                Altura = altura,
                Edad = edad,
                EsFumador = esFumador,
                NumHijos = numHijos,
                Peso = peso,
                Region = region
            };
            return prediccion.PredecirCostoDeSeguro();
        }
    }
}
