﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MedicaHuichapan.BIZ;
namespace MedicaHuichapan.Escritorio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PrediccionGastoMedico prediccion;
        public MainWindow()
        {
            InitializeComponent();
            prediccion = (PrediccionGastoMedico)this.DataContext;
        }

        private void btnEstimarCosto_Click(object sender, RoutedEventArgs e)
        {
            lblCosto.Content = $"${prediccion.PredecirCostoDeSeguro()}";
        }
    }
}
