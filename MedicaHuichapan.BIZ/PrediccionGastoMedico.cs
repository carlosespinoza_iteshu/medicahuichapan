﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicaHuichapan.BIZ
{
    public class PrediccionGastoMedico
    {
        public int Edad { get; set; }
        public double Peso { get; set; }
        public double Altura { get; set; }
        public int NumHijos { get; set; }
        public bool EsFumador { get; set; }
        public char Sexo { get; set; }
        public int Region { get; set; }

        private double ObtenerBMI()
        {
            return Peso / Math.Pow(Altura, 2);
        }

        public double PredecirCostoDeSeguro()
        {
            //y=(256.85635254)(age)+(339.19345361)(bmi)+(475.50054515)(children)+(23848.53454191)(smoker)+(65.6571797)(sexFemale)−(65.6571797)(sexMale)+(587.00923503)(regionNortheast)+(234.0453356)(regionSorthwest)−(448.01281436)(regionSouthest)−(373.04175627)(southWest)
            return 2.568535254 * Edad + 339.19345361 + ObtenerBMI() + 475.50054515 * NumHijos + (EsFumador ? 23848.53454191 : 0) + (Sexo == 'M' ? -65.6571797 : 65.6571797) + SumarRegion();
        }

        private double SumarRegion()
        {
            //1.-  regionNortheast
            //2.-  regionSorthwest
            //3.-  regionSouthest
            //4.-  regionSouthWest
            switch (Region)
            {
                case 1:
                    return 587.00923503;
                case 2:
                    return 234.0453356;
                case 3:
                    return -448.01281436;
                case 4:
                    return -373.04175627;
                default:
                    return 0;
            }
        }
    }
}
